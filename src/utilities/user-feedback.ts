import {LoadingController,Loading,AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';


@Injectable()
export class UserFeedback{
  loader:Loading;
 constructor(private localNotifications:LocalNotifications,public toastCtrl: ToastController,public alertCtrl:AlertController,public loadingCtrl:LoadingController){
   this.loader = this.loadingCtrl.create({
       content: "Please wait...",
       dismissOnPageChange:true
   });
 }

  start_progress(){
     this.loader=this.loadingCtrl.create({
        content: "إنتظر من فضلك ...",
        dismissOnPageChange:true
    });
    this.loader.present();
  }

  close_progress(){
    this.loader.dismiss();
  }
  alert(title:string,subtitle:string) {
     let msg = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Okay']
     });
     msg.present().catch(err=>{});
  }

  make_toast(msg:string,cssClass:string="toast",duration=3000){
    let toast = this.toastCtrl.create({
       message: msg,
       duration: duration,
       position: 'top',
       cssClass:cssClass
     });
     toast.present().catch(err=>{});
  }



 presentConfirm(cb) {
  let alert = this.alertCtrl.create({
    title: 'Confirm',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel'
      },
      {
        text: 'Ok',
        handler: () => {
          cb();
        }
      }
    ]
  });
  alert.present().catch(err=>{});
}

  generate_notification(id,title){
    this.localNotifications.schedule({
      id: id,
      title:title,
      icon:"assets/icon.png",
      smallIcon:"assets/icon.png",
      color:'#d62d20'
    });
    this.make_toast(title);
  }

  convertToDataURLviaCanvas(url, outputFormat){
  	return new Promise( (resolve, reject) => {
  		let img = new Image();
  		img.crossOrigin = 'Anonymous';
  		img.onload = function(){
  			let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
  			ctx = canvas.getContext('2d'),
  			dataURL;
  			canvas.height = img.height;
  			canvas.width = img.width;
  			ctx.drawImage(img, 0, 0);
  			dataURL = canvas.toDataURL(outputFormat);
  			//callback(dataURL);
  			canvas = null;
  			resolve(dataURL);
  		};
  		img.src = url;
  	});
  }
}
