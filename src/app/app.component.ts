import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';
import { LoginPage } from '../pages/login/login';
import { ShareAppPage } from '../pages/share-app/share-app';
import { NewsPage } from '../pages/news/news';
import { Storage } from '@ionic/storage';
import { AuthService } from '../providers/auth-service';
import * as io from 'socket.io-client';
import { NativeAudio } from '@ionic-native/native-audio';

@Component({
  templateUrl: 'app.html',
  providers:[AuthService,NativeAudio]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  user:any={};
  socket:any;
  pages: Array<{title: string, component: any,icon:string}>;

  constructor(private nativeAudio:NativeAudio,private events:Events,private authService:AuthService,private storage:Storage,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.nativeAudio.preloadSimple('goal', 'assets/goal.m4a').then(()=>{},err=>{}).catch(err=>{});
    this.socket=io.connect("http://97.74.4.85:5000");

    this.socket.on('new_event',data=>{
      this.events.publish(data.event_name,data);
    });
    events.subscribe('login',(user)=>{
      this.user=user;
      this.socket.emit('join',user._id);
    });

    this.storage.get('user').then(user=>{
      this.user=user;
      if(user){
        this.socket.emit('join',user._id);
        this.rootPage=HomePage;
      }else{
        this.rootPage=LoginPage;
      }
    })
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'المباريات', component: HomePage,icon:'football' },
      { title: 'الاخبار', component: NewsPage ,icon:'document'},
      { title: 'شارك الابليكشن', component: ShareAppPage,icon:'md-share' },
      { title: 'الاعدادات', component: SettingsPage,icon:'settings' },
      { title: 'تسجيل الخروج', component: undefined,icon:'log-out' },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(!page.component)
      this.logout();
    else
      this.nav.setRoot(page.component);
  }

  logout(){
    this.storage.clear();
    this.authService.update_token(this.user._id,"").then(()=>{});
    this.nav.setRoot(LoginPage);
  }
}
