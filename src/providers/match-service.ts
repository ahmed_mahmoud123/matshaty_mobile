import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class MatchService {
  headers:Headers;
  api_url:string="/api/matches";
  constructor(public http: Http) {
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
    this.api_url="http://97.74.4.85:5000/matches";
  }



  get_matches(teams,day){
    return this.http.post(`${this.api_url}/get`,{
      teams:teams,
      day:day
    },{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }
}
