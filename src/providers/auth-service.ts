import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {
  headers:Headers;
  api_url:string="/api/users";
  constructor(public http: Http) {
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
    this.api_url="http://97.74.4.85:5000/users";
  }

  login(user){
      return this.http.post(`${this.api_url}/login`,JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  update_token(user_id,token){
      return this.http.post(`${this.api_url}/update_token`,JSON.stringify({
        user_id:user_id,
        token:token
      }),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }

  teams(teams,user_id){
      return this.http.post(`${this.api_url}/teams`,JSON.stringify({
        _id:user_id,
        teams:teams
      }),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }
}
