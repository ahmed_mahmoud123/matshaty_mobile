import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class TeamService {
  headers:Headers;
  api_url:string="/api/teams";
  constructor(public http: Http) {
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
    this.api_url="http://97.74.4.85:5000/teams";

  }


  add_teams_to_user(teams,user_id){
      return this.http.post(`${this.api_url}/add_to_user`,JSON.stringify({teams:teams,user_id:user_id}),{headers:this.headers});
  }

  get_teams(){
    return this.http.get(`${this.api_url}/list`,{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }
}
