export class Match{
  home:string;
  away:string;
  championship:string;
  datetime:Date;
}
