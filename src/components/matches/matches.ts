import { Component,Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser';
import { UserFeedback } from '../../utilities/user-feedback';
import { MatchService } from '../../providers/match-service';
import { Storage } from '@ionic/storage';
import { NativeAudio } from '@ionic-native/native-audio';

/**
 * Generated class for the MatchesComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'matches',
  templateUrl: 'matches.html',
  providers:[UserFeedback,MatchService,NativeAudio]
})

export class MatchesComponent {
  matches:Array<any>=[];
  @Input('loading') loading:boolean;
  @Input('day') day:number;


  constructor(private nativeAudio: NativeAudio,private storage:Storage,private matchService:MatchService,private userFeedback:UserFeedback,private domSanitizer:DomSanitizer,private events:Events) {
    storage.get('user').then(user=>{
      this.loading=true;
      this.matchService.get_matches(user.teams,this.day).then(matches=>{
        this.matches=matches.map(match=>{
          let date=new Date(match.datetime);
          let hours=date.getHours()<10?"0"+date.getHours():date.getHours();
          let minutes=date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes();
          match.time=`${hours}:${minutes}`;
          if(match.link)
            match.link=this.domSanitizer.bypassSecurityTrustResourceUrl(match.link);
          return match;
        });
        this.loading=false;
     })
    })


    this.events.subscribe('new_match',match=>{

    });

    this.events.subscribe('edit_match',data=>{
      let index=-1;
      for (let i = 0; i < this.matches.length; i++) {
          if(this.matches[i]._id == data.match._id){
            index=i;
          }
      }
      if(index != -1 ){
        let match=this.matches[index];

        if(match.result != data.match.result){
          match.result = data.match.result
          this.userFeedback.make_toast(`${match.home.name} - ${match.away.name} \n جوووووووول`,'success',6000);
          this.nativeAudio.play('goal').then(()=>{});
        }
        if(data.match.link_change){
          match.link=this.domSanitizer.bypassSecurityTrustResourceUrl(data.match.link);
        }
        if(match.status != data.match.status){
            match.status = data.match.status;
            switch(data.match.status){
              case 1:
                this.userFeedback.make_toast(`${match.home.name} - ${match.away.name} \n المباره بدأت`,'success');
              break;
              case 0:
                this.userFeedback.make_toast(`${match.home.name} - ${match.away.name} \n إنتهت المباره`,'success');
            }
        }
      }
    });
  }




}
