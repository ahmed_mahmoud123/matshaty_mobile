import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { TeamService } from '../../providers/team-service';
import { Storage } from '@ionic/storage';
import {AuthService} from '../../providers/auth-service';
import { UserFeedback } from '../../utilities/user-feedback';
import { HomePage } from '../home/home';

/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers:[TeamService,AuthService,UserFeedback]
})
export class SettingsPage {

  teams:Array<any>=[];
  user:any;
  constructor(private userFeedback:UserFeedback,private authService:AuthService,private storage:Storage,private teamService:TeamService,public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    this.storage.get('user').then(user=>{
      this.user=user;
      this.teamService.get_teams().then(teams=>{
        let teams_str=this.user.teams.join(',');
        for (let i = 0; i < teams.length; i++) {
            if(teams_str.indexOf(teams[i]._id)>=0)
              teams[i].checked=true;
        }
        this.teams=teams;
      });
    });
  }

  save(){
    this.userFeedback.start_progress();
    this.user.teams=this.teams.filter(team=>{return team.checked}).map(team=>team._id);
    this.authService.teams(this.user.teams,this.user._id).then(result=>{
      this.userFeedback.close_progress();
      this.user.teams=this.user.teams;
      this.storage.set('user',this.user);
      this.userFeedback.make_toast('تم التعديل','success');
      this.navCtrl.setRoot(HomePage);
    },err=>{
      this.userFeedback.close_progress();
      this.userFeedback.make_toast('يوجد خطأ في التعديل نرجو المحاوله لاحقا');
    }).catch(err=>{})
  }

}
