import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { ShareAppPage } from '../share-app/share-app';
import { AuthService } from '../../providers/auth-service';
import { Storage } from '@ionic/storage';

import {
  Push,
  PushToken
} from '@ionic/cloud-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[AdMobFree,AuthService,YoutubeVideoPlayer]
})

export class HomePage {
  matches_day:string="today";
  loading:boolean=false;
  user:any={};
  constructor(private authService:AuthService,public push:Push,private storage:Storage,private youtube: YoutubeVideoPlayer,private admobFree: AdMobFree,public navCtrl: NavController) {
    storage.get('user').then(user=>{
      this.user=user;
      if(!user.token){
        this.update_device_token(user._id);
      }

    });

    const bannerConfig: AdMobFreeBannerConfig = {
     // add your config here
     // for the sake of this example we will just use the test config
     id:"ca-app-pub-6020261032617075/2758189747",
     //isTesting: true,
     autoShow: true
   };
   this.admobFree.banner.config(bannerConfig);
   this.admobFree.banner.prepare().then(() => {}).catch(e => console.log(e));
  }

  ionViewDidLoad() {
  }

  share(){
    this.navCtrl.push(ShareAppPage);
  }

  update_device_token(user_id){
    this.push.register().then((t: PushToken) => {
      this.authService.update_token(user_id,t.token).then(()=>{
        this.user.token=t.token;
        this.storage.set('user',this.user);
      });
      return this.push.saveToken(t);
    }).then((t: PushToken) => {
      console.log('Token saved:', t.token);
    });
  }

}
