import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import {UserFeedback} from '../../utilities/user-feedback';

/**
 * Generated class for the ShareAppPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-share-app',
  templateUrl: 'share-app.html',
  providers:[SocialSharing,UserFeedback]

})
export class ShareAppPage {

  messages:string="تطبيق ماتشاتي لمتابعة مواعيد مباريات فرقك المفضله ومشاهده المباريات في مواعيده بشكل مباشر ومتابعة الاخبار المتعلقه بفرقك المفضله";
  title:string="ماتشاتي";
  image:any="assets/icon.png";
  url:string="http://www.ahmedkesha.com";
  constructor(private userFeedback:UserFeedback,private socialSharing:SocialSharing,public navCtrl: NavController, public navParams: NavParams) {
    userFeedback.convertToDataURLviaCanvas("assets/icon.png", "image/png")
    .then( base64Img =>
      {
        this.image=base64Img;
        this.socialSharing.share(this.messages,this.title,this.image,this.url).then(rs=>{});
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShareAppPage');
  }

  share_facebook(){
    this.socialSharing.shareViaFacebookWithPasteMessageHint("",this.image,this.url,this.messages).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Facebook");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Facebook");
    })
  }


  share_twitter(){
    this.socialSharing.shareViaTwitter(this.messages,this.image,this.url).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Twitter");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Twitter");
    })
  }

  share_instgram(){
    this.socialSharing.shareViaInstagram(this.messages,this.image).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Instagram");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Instagram");
    })
  }

  share_sms(){
    this.socialSharing.shareViaSMS(this.messages,"").then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using SMS");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using SMS");
    })
  }

  share_whatsapp(){
    this.socialSharing.shareViaWhatsApp(this.messages,this.image,this.url).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using What's app");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using  What's app");
    })
  }

  share_email(){
    this.socialSharing.shareViaEmail(this.messages,this.title,[""]).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Email");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Email");
    })
  }

}
