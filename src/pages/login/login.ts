import { Component } from '@angular/core';
import { IonicPage,MenuController, NavController, NavParams,Events } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { HomePage } from '../home/home';

import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import {AuthService} from '../../providers/auth-service';
import { UserFeedback } from '../../utilities/user-feedback';
import { User } from '../../Models/User';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[AuthService,Facebook,GooglePlus,UserFeedback]
})
export class LoginPage {
  user:User=new User();
  constructor(private events:Events,private navCtrl:NavController,private userFeedback:UserFeedback,private storage:Storage,private googlePlus:GooglePlus,private facebook:Facebook,private authService:AuthService,private menuCtrl:MenuController) {
    this.menuCtrl.enable(false);
  }






  facebook_login(){
    // set scope permission for facebook application
    const permissions=['public_profile','email'];
    // request permiossions and get access token ..
    this.facebook.login(permissions).then(user=>{
      // send new request with access token to get user information and send empty params becuase we send using get method
      this.facebook.api("/me?fields=id,name,email,picture.width(300).height(300)",[]).then(user=>{
        // display user data
        this.user.email=user.email;
        this.user._id=user.id;
        this.user.name=user.name;
        this.user.avatar=user.picture.data.url;
        //this.userFeedback.start_progress();
        this.authService.login(this.user).then(user=>{
          this.storage.set('user',user);
          this.userFeedback.close_progress();
          this.events.publish('login',user);
          this.menuCtrl.enable(true);
          this.navCtrl.setRoot(SettingsPage);
        },err=>{
          console.log(JSON.stringify(err));
          this.userFeedback.close_progress();
          this.userFeedback.make_toast('تسجيل دخول خاطئ');
        }).catch(err=>{
          console.log(JSON.stringify(err));
          this.userFeedback.close_progress();
          this.userFeedback.make_toast('تسجيل دخول خاطئ');
        })
      }).catch(err=>{})
    }).catch(err=>{});
  }


  google_login(){
    // call google api to request accept premissions to get user info
    this.userFeedback.start_progress();
    this.googlePlus.login({scopes:"profile"}).then(user=>{
      this.user.name=`${user.givenName} ${user.familyName}`;
      this.user._id=user.userId;
      this.user.email=user.email;
      this.user.avatar=user.imageUrl;
      this.authService.login(this.user).then(result=>{
        this.storage.set('user',user).then(()=>{
          this.userFeedback.close_progress();
          this.events.publish('login');
          this.menuCtrl.enable(true);
          this.navCtrl.setRoot(SettingsPage);
        });
      },err=>{
          this.userFeedback.close_progress();
          this.userFeedback.make_toast('تسجيل دخول خاطئ');
      })
    }).catch(err=>{
      this.userFeedback.close_progress();
      this.userFeedback.make_toast('تسجيل دخول خاطئ');
    })
  }

}
